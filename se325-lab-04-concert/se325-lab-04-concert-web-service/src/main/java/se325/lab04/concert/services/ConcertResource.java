package se325.lab04.concert.services;

import se325.lab04.concert.domain.Concert;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import java.net.URI;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Path("/concerts")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ConcertResource {

    @GET
    @Path("{id}")
    public Response retrieveConcert(@PathParam("id") long id) {
        Concert concert;

        // Acquire an EntityManager (creating a new persistence context).
        EntityManager em = PersistenceManager.instance().createEntityManager();
        try {
            // Start a new transaction.
            em.getTransaction().begin();
            // Use the EntityManager to retrieve, persist or delete object(s).
            // Use em.find(), em.persist(), em.merge(), etc...
            concert = em.find(Concert.class, id);
            // Commit the transaction.
            em.getTransaction().commit();
        } finally {
            // When you're done using the EntityManager, close it to free up resources.
            em.close();
        }

        if (concert == null) {
            // Return a HTTP 404 response if the specified Concert isn't found.
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        ResponseBuilder builder = Response.ok(concert);
        return builder.build();
    }

    @POST
    public Response createConcert(Concert concert) {
        System.out.println("*******************************************************");
        System.out.println("The following ID should be null, is it?:" + concert.getId());
        System.out.println("*******************************************************");
        EntityManager em = PersistenceManager.instance().createEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(concert);
            em.getTransaction().commit();
        } finally {
            // When you're done using the EntityManager, close it to free up resources.
            em.close();
        }
        System.out.println("*******************************************************");
        System.out.println("The following ID should be null, is it?:" + concert.getId());
        System.out.println("*******************************************************");
        ResponseBuilder builder = Response.created(URI.create("/concerts/" + concert.getId()));
        return builder.build();
    }

    @PUT
    public Response updateConcert(Concert concert) {
        EntityManager em = PersistenceManager.instance().createEntityManager();
        try {
            em.getTransaction().begin();
            Concert foundConcert = em.find(Concert.class, concert.getId());
            if (foundConcert == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }
            em.merge(concert);
            em.getTransaction().commit();
        } finally {
            // When you're done using the EntityManager, close it to free up resources.
            em.close();
        }

        ResponseBuilder builder = Response.status(Response.Status.NO_CONTENT);
        return builder.build();
    }

    @DELETE
    @Path("{id}")
    public Response deleteConcert(@PathParam("id") long id) {
        Concert concert;
        EntityManager em = PersistenceManager.instance().createEntityManager();
        try {
            em.getTransaction().begin();
            concert = em.find(Concert.class, id);
            if (concert == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }
            em.remove(concert);
            em.getTransaction().commit();
        } finally {
            // When you're done using the EntityManager, close it to free up resources.
            em.close();
        }

        ResponseBuilder builder = Response.status(Response.Status.NO_CONTENT);
        return builder.build();
    }

    @DELETE
    public Response deleteAllConcerts() {
        EntityManager em = PersistenceManager.instance().createEntityManager();
        TypedQuery<Concert> concertQuery = em.createQuery("select c from Concert c", Concert.class);
        List<Concert> concerts = concertQuery.getResultList();
        try {
            em.getTransaction().begin();
            for (Concert currentConcert : concerts) {
                em.remove(currentConcert);
            }
            em.getTransaction().commit();
        } finally {
            // When you're done using the EntityManager, close it to free up resources.
            em.close();
        }

        ResponseBuilder builder = Response.status(Response.Status.NO_CONTENT);
        return builder.build();
    }
}
